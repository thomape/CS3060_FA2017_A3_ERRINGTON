import scala.io.Source
import java.io.{BufferedWriter, FileWriter}
import scala.collection.JavaConversions._
import scala.collection.mutable.ListBuffer
import scala.util.Random



object funcA3 {
	def main(args: Array[String]){

		// Function Calls
		// Problem One
		problemOne()

		// Problem Two
		var myString = "Book"
		var mySubStr = "ook"
		problemTwo(myString, mySubStr )
		}


		// Problem Three
		val randArray = Array.fill(100) (100).map(scala.util.Random.nextInt)
		problemThree(randArray)

		// Problem Four
		problemFour(randArray)

		// Problem Five

		val fibs1 = problemFive(10)
		println(fibs1)


		// Problem Six.

		val  fibs2 = problemSix(10)
		println(fibs2)

		// Problem Seven
		val sizeOf = 100 
		// TODO

		// Problem Eight
		var location = "HammingData.csv"
		problemEight(location)

		// problem Nine
		var test = "test.csv"
		problemNine(test)


		// Function Definitions

		def problemOne() {
			println("Hello World")
		}

		def problemTwo(myString : String, mySubStr : String) {
			println(myString)
			println(mySubStr)

			if (myString.contains(mySubStr)) {
				var contains = myString.indexOf(mySubStr)
				println("Substring starts at index " + contains)
			}
			else{
				println("Does not contain substring")
			}
			
		}

		def problemThree(randArray : Array[Int]) {
			randArray.foreach {println}

			println("---rev------")
			val revList = randArray.reverse
			revList.foreach {println}

			println("---odds------")
			
			val odds = (1 to 51 by 2)
			val oddList = odds.map(revList)
			oddList.foreach {println}
		}

		def problemFour(randArray : Array[Int]) {

			println("---Sort---")

			for (i <- (0 until randArray.length).reverse) {
				var max = i 
				for (j <- (0 until i).reverse) {
					if (randArray(j) > randArray(max)) max = j
				}
				val temp = randArray(i)
				randArray(i) = randArray(max)
				randArray(max) = temp

			}
			randArray.foreach {println}
			


		}


		def problemFive( num : Int ) : Int = {

			//val t0 = System.nanoTime()
			println("----Fibs----")
			var first = 0
			var middle = 1
			var last = 0

			while (last < num) {
				val holder = first + middle
				first = middle
				middle = holder
				last = last + 1
			}
			return first

			//val t1 = System.nanoTime()
			//val t2 = t1 - t0
			//println(t2)
		}

		def problemSix( num : Int) : Int = { 

			println("---Fibs---")
 			 def tailRec( num: Int, next:Int, last:Int): Int = num match {
    			case 0 => next 
    			case _ => tailRec( num-1, last, next+last )
  				}
  				return tailRec( num, 0, 1)
				}


		//def problemSeven
		//TODO
		
		// Problem Eight

		def problemEight( fileLocation : String ) {
			val lines = Source.fromFile(fileLocation).getLines.toArray
			lines.foreach {println}

			var hamming = 0
			var myLen = lines(0).length
			
			var i = 1

			for  (i <- 1 to 4){

				if (lines(0).charAt(i) != lines(1).charAt(i)) {
					hamming = hamming + 1;

					println(hamming)

			}
/*
			// Cant get the output to work
			val outputFile = (fileLocation + "_OutPut.csv")
			
			var first = lines(0).toString
			var second = lines(1).toString

			val myList = List(first, ":", second, ":",  hamming)
			
			val writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream
				(outputFile)))
			
				writer.write( first + ":" + second + ":" + hamming)*/
			
				
			
				
			}
			
		}

		def problemNine	(test : String)	{
			
			val daList = Source.fromFile(test).getLines.toArray
			daList.foreach {println}
		}

	


}


	// class Shape {

	// 		// class vars
			
	// 		var color: String = ""
	// 		var filled: Boolean = false	

	// 		// default
	// 		def shape() {
	// 			color = "Red"
	// 			filled = false
	// 		}

	// 		// // overload
	// 		// def shapeMine(aColor : var , isFilled : var) {
	// 		// 	Shape.color = aColor
	// 		// 	Shape.filled = isFilled
	// 		// }

	// 		def getColor(){
	// 			println(color)
	// 			return color
	// 		}

	// 		// def setColor(newColor : String):  {
	// 		// 	color = newColor
	// 		// }

	// 		// def isFilled() {
	// 		// 	return filled
	// 		// }

	// 		// def setFilled(fill : var){
	// 		// 	fill = filled
	// 		// }

	// 	}

class Circle extends Shape {
	var radius: Double = 1.0


}

class Rectangle extends Shape {
	var width: Double = 1.0
	var length: Double = 1.0
}

class Square extends Rectangle {
	// Stuff
}